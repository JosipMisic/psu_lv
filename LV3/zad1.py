import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# 1.
#mtcars = pd.read_csv('mtcars.csv')
#print(mtcars.sort_values(by=['mpg']).head(5)['car'])

# 2.
#mtcars = pd.read_csv('mtcars.csv')
#print(mtcars[mtcars.cyl==8].sort_values(by=['mpg']).tail(3))

# 3.
#mtcars = pd.read_csv('mtcars.csv')
#print(mtcars[mtcars.cyl==6].mpg.mean())

# 4.
#mtcars = pd.read_csv('mtcars.csv')
#print(mtcars[(mtcars.cyl==4) & (mtcars.wt>2.0) & (mtcars.wt<2.2)].mpg.mean())

# 5.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars[(mtcars.am == 1)].am
#print("Broj automobila s manualnim mjenjacem: ", mtcars_new.sum())
#sum = mtcars_new.sum()
#print("Broj automobila s automatskim mjenjacem: ", len(mtcars)-sum)

# 6.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars_new = mtcars[(mtcars.am == 0) & (mtcars.hp > 100)].am
#print("Broj automobila s automatskim mjenjacem: ", len(mtcars_new))

# 7.
mtcars = pd.read_csv('mtcars.csv')
mtcars['kg']= mtcars['wt']*1000.0 * 0.453
print(mtcars)