import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# 1.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars.groupby('cyl').mpg.mean().plot.bar()
#plt.show()

# 2.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars.boxplot(column = 'wt', by = 'cyl')
#plt.show()

# 3.
#mtcars = pd.read_csv('mtcars.csv')
#mtcars.boxplot(column = "mpg", by = "am")
#plt.show()

# 4.
mtcars = pd.read_csv('mtcars.csv')
ax = mtcars[mtcars.am==0].plot.scatter(x='qsec',y='hp',color='Blue',label='0')
mtcars[mtcars.am==1].plot.scatter(x='qsec',y='hp',color='red',label='1',ax=ax)
plt.show()

