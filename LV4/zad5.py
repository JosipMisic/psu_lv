import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error, max_error
from sklearn.linear_model import LinearRegression

df = pd.read_csv('cars_processed.csv')
print(df.info())

#izbacivanje nepotrebnog
df = df.drop(['name', 'mileage'], axis=1)

#odabir ulaza
X = df[['km_driven','year','engine', 'max_power']]
Y = df['selling_price']

#podjela train i test
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=10)

#skaliranje
Scaler = MinMaxScaler()
X_train_s = Scaler.fit_transform(X_train)
X_test_s = Scaler.transform(X_test)

#izrada modela
linear_model = LinearRegression()
linear_model.fit(X_train_s, Y_train)

#evaluacija modela
y_pred_train = linear_model.predict(X_train_s)
y_pred_test = linear_model.predict(X_test_s)

print('R2 test: ', r2_score(y_pred_test, Y_test))
print('RMSE test: ',np.sqrt(mean_absolute_error(y_pred_test, Y_test)))
print('Max error test: ', max_error(y_pred_test, Y_test))
print('MAE test: ', mean_absolute_error(y_pred_test, Y_test))
