import numpy as np
import matplotlib.pyplot as plt
br1 = 0
br2 = 0
br3 = 0
br4 = 0
br5 = 0
br6 = 0
i = 0

podaci = np.empty(1, int)

for i in range (100):
    podaci = np.append(podaci, np.random.randint(1,7))


for i in range(0,100,1):
    if podaci[i] == 1:
        br1=br1+1
    if podaci[i] == 2:
        br2=br2+1
    if podaci[i] == 3:
        br3=br3+1
    if podaci[i] == 4:
        br4=br4+1
    if podaci[i] == 5:
        br5=br5+1
    if podaci[i] == 6:
        br6=br6+1

print("Broj 1: ", br1)
print("Broj 2: ", br2)
print("Broj 3: ", br3)
print("Broj 4: ", br4)
print("Broj 5: ", br5)
print("Broj 6: ", br6)


plt.xlabel('Kockica')
plt.ylabel('Broj ponavljanja')
plt.title('Histogram bacanja kockice')
plt.hist(podaci, bins=10, align = 'left')
plt.axis([0, 7, 0, 30])
plt.show()
